# RFID Python

RFID using OrangePi with Pyhon


Note that armbain-config does not set this parameter, so you can't get SPI working using this tool only. So for now it's easier just to edit armbianEnv.txt directly.
location 
cd .. 
boot/

verbosity=1
logo=disabled
console=both
disp_mode=1920x1080p60
overlay_prefix=sun8i-h3
rootdev=UUID=09e9478d-c7f2-4b16-a2d7-66f5313ff813
rootfstype=ext4
overlays=spi-spidev
param_spidev_spi_bus=0
usbstoragequirks=0x2537:0x1066:u,0x2537:0x1068:u